#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include"pessoa.hpp"
#include<string>

class Professor : public Pessoa{

    private:
        string formacao;
        string sala;
        float salario;

    public:
        Professor();
        ~Professor();

        void setFormacao(string formacao);
        string getFormacao();
        void setSala(string sala);
        string getSala();
        void setSalario(float salario);
        float getSalario();

        void imprimeDadosProfessor();

};

#endif
