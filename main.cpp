#include <iostream>
#include <string>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

#define tam 20

using namespace std;

struct Ficha_Pessoa{
    string nome;
    string matricula;
    int idade;
    string sexo;
    string telefone;
    float ira;
    int semestre;
    string curso;
    string formacao;
    float salario;
    string sala;
};
struct Ficha_Pessoa ficha[tam];

void introducao(){
    cout << "----------------------------------------------" << endl;
    cout << "Selecione o perfil" << endl;
    cout << "Sair\t\t\t(0)" << endl;
    cout << "Aluno\t\t\t(1)" << endl;
    cout << "Professor\t\t(2)" << endl;
    cout << "----------------------------------------------" << endl;
}

void cadastro_professor(){
    int i,num;
    Professor *lista_de_professor[tam];
    cout << "----------------------------------------------" << endl;
    cout << "Digite quantidade de professor a ser cadastrado:" << endl;
    cin >> num;
    for (i = 0; i < num; i++) {
        cout << "----------------------------------------------" << endl;
        cin.ignore();
        cout << "Digite seu Nome:" << endl;
        getline(cin,ficha[i].nome);
        cout << "Digite sua Matricula:" << endl;
        getline(cin,ficha[i].matricula);
        cout << "Digite sua Idade:" << endl;
        cin >> ficha[i].idade;
        cin.ignore();
        cout << "Digite seu sexo:" << endl;
        getline(cin,ficha[i].sexo);
        cout << "Digite seu telefone:" << endl;
        getline(cin,ficha[i].telefone);
        cout << "Digite sua formação:" << endl;
        getline(cin,ficha[i].formacao);
        cout << "Digite seu salario:" <<  endl;
        cin >> ficha[i].salario;
        cin.ignore();
        cout << "Digite o sua sala:" << endl;
        cin >> ficha[i].sala;
        cout << "----------------------------------------------" << endl;

        lista_de_professor[i] = new Professor();
        lista_de_professor[i] -> setNome(ficha[i].nome);
        lista_de_professor[i] -> setMatricula(ficha[i].matricula);
        lista_de_professor[i] -> setIdade(ficha[i].idade);
        lista_de_professor[i] -> setSexo(ficha[i].sexo);
        lista_de_professor[i] -> setTelefone(ficha[i].telefone);
        lista_de_professor[i] -> setFormacao(ficha[i].formacao);
        lista_de_professor[i] -> setSalario(ficha[i].salario);
        lista_de_professor[i] -> setSala(ficha[i].sala);

    }
    for (i = 0; i < num ; i++) {
        lista_de_professor[i] -> imprimeDadosProfessor();
    }
}

void cadastro_alunos(){
    int i,num;
    Aluno *lista_de_alunos[tam];
    cout << "Digite quantidade de aluno a ser cadastrado:" << endl;
    cin >> num;
    for (i = 0; i < num; i++) {
        cout << "----------------------------------------------" << endl;
        cin.ignore();
        cout << "Digite seu Nome:" << endl;
        getline(cin,ficha[i].nome);
        cout << "Digite sua Matricula:" << endl;
        getline(cin,ficha[i].matricula);
        cout << "Digite sua Idade:" << endl;
        cin >> ficha[i].idade;
        cout << "Digite seu sexo:" << endl;
        cin.ignore();
        getline(cin,ficha[i].sexo);
        cout << "Digite seu telefone" << endl;
        getline(cin,ficha[i].telefone);
        cout << "Digite seu ira:" <<  endl;
        cin >> ficha[i].ira;
        cout << "Digite o seu semestre:" << endl;
        cin >> ficha[i].semestre;
        cout << "Digite o seu curso:" << endl;
        cin.ignore();
        getline(cin,ficha[i].curso);
        cout << "----------------------------------------------" << endl;

        lista_de_alunos[i] = new Aluno();
        lista_de_alunos[i] -> setNome(ficha[i].nome);
        lista_de_alunos[i] -> setMatricula(ficha[i].matricula);
        lista_de_alunos[i] -> setIdade(ficha[i].idade);
        lista_de_alunos[i] -> setSexo(ficha[i].sexo);
        lista_de_alunos[i] -> setTelefone(ficha[i].telefone);
        lista_de_alunos[i] -> setIra(ficha[i].ira);
        lista_de_alunos[i] -> setSemestre(ficha[i].semestre);
        lista_de_alunos[i] -> setCurso(ficha[i].curso);

    }
    for (i = 0; i < num ; i++) {
        lista_de_alunos[i]-> imprimeDadosAluno();
    }
}


int main(int argc, char ** argv) {

   int escolha=3;
   while(escolha!=0){
   introducao();
   cin >> escolha;
   if (escolha == 1) {
       cadastro_alunos();
   }
    else if(escolha == 2){
    cadastro_professor();

    }
    }
/*
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();
*/
   /*
   cout << "Nome: ";
   cin >> nome;
   cout << "Matricula: ";
   cin >> matricula;
   cout << "idade: ";
   cin >> idade;

   lista_de_aluno[0] = new Aluno();

   lista_de_aluno[0]->setNome(nome);
   lista_de_aluno[0]->setMatricula(matricula);
   lista_de_aluno[0]->setIdade(idade);
   lista_de_aluno[0]->imprimeDadosAluno();
   */
  // delete(pessoa_3);
  // delete(pessoa_4);

   return 0;
}
/*
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();

   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);
   */
    /*
   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);

   pessoa_3->setNome("Pateta");//PONTEIRO USASSE A ->
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefo:ne("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);
   */


/*
   Aluno aluno_1;

   Professor professor_1;

   //cout << " Curso do aluno " << pessoa_1.getNome()<< " : " << aluno_1.getCurso() << endl;
   */

   /*
   //cout << "Nome: " << pessoa_1.nome; << endl;
   cout << "Nome: " << pessoa_1.getNome() << endl;
   cout << "Matrícula: " << pessoa_1.getMatricula() << endl;
   cout << "Telefone: " << pessoa_1.getTelefone() << endl;
   cout << "Sexo: " << pessoa_1.getSexo() << endl;
   cout << "Idade: " << pessoa_1.getIdade() << endl;

   cout << endl;
   cout << "Nome: " << pessoa_2.getNome() << endl;
   cout << "Telefone: " << pessoa_2.getTelefone() << endl;
   cout << "Idade: " << pessoa_2.getIdade() << endl;

   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();
   */

   //delete(pessoa_3);//SE PAROU DE USAR O PONTEIRO, SEMPRE BOM ENCERRAR
   //delete(pessoa_4);
