#include"professor.hpp"
#include<iostream>

using namespace std;

Professor::Professor(){
    setFormacao("");
    setSalario(1000.0);
    setSala("");

}

Professor::~Professor(){}

void Professor::setFormacao(string formacao){
    setFormacao(formacao);

}
string Professor::getFormacao(){
    return formacao;

}
void Professor::setSalario(float salario){
    setSalario(salario);

}
float Professor::getSalario(){
    return salario;

}
void Professor::setSala(string sala){
    setSala(sala);

}
string Professor::getSala(){
    return sala;

}

void Professor::imprimeDadosProfessor(){
    cout << "----------------------------------------------" << endl;
    cout << "Dados do Professor" << endl;
    imprimeDados();
    cout << "Formacao: " << getFormacao() << endl;
    cout << "Salario: R$" << getSalario() << endl;
    cout << "Sala: " << getSala() << endl;
    cout << "----------------------------------------------" << endl;

}
